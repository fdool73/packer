#!/bin/bash
aws s3 cp s3://S3BUCKETNAME/Linux/Build-Amazon-Linux.sh .
aws s3 cp s3://S3BUCKETNAME/Linux/Amazon-Linux-Build.json .
export ACCOUNT=$(aws ssm get-parameter --name "Accounts parameter" --query "Parameter.Value" --output text --region us-east-1)
export LINUXAMI=$(aws ssm get-parameter --name "SourceAmazonLinux" --query "Parameter.Value" --output text --region us-east-1)
PACKER_LOG_PATH="amazon-linux-build-log.txt" PACKER_LOG=1 packer build Amazon-Linux-Build.json
aws s3 cp amazon-linux-build-log.txt s3://loggingbucket
